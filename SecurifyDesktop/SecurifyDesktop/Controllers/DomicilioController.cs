﻿using Newtonsoft.Json;
using SecurifyDesktop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyDesktop.Controllers
{
    class DomicilioController
    {
        public async Task<Domicilio> GetDomicilioID(Domicilio domicilio)
        {
            Domicilio _domicilio = new Domicilio();
            HttpClient client = new HttpClient();
            var json = JsonConvert.SerializeObject(domicilio);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Helpers.apiUrl + "domicilio/getid", content);
            string jsonString = await response.Content.ReadAsStringAsync();
            return _domicilio = JsonConvert.DeserializeObject<Domicilio>(jsonString);
        }
    }
}
