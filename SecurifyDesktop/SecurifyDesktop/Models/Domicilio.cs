﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurifyDesktop.Models
{
    public class Domicilio
    {
        public int id { get; set; }
        public int idCalle { get; set; }
        public string numero { get; set; }
        public string tipo { get; set; }
    }
}
