﻿namespace SecurifyDesktop.Views
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.UsuariosPage = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.UBloqueoDesbloqueoPage = new System.Windows.Forms.TabPage();
            this.btnGuardarUBUS = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbEstatusUBUS = new System.Windows.Forms.ComboBox();
            this.txtNombreUBUS = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtIDUBUS = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDomicilioUBUS = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnBuscarUsuarioUBUS = new System.Windows.Forms.Button();
            this.cmbUsuariosUSUB = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNombreBusUBUS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEnviarChatUSU = new System.Windows.Forms.Button();
            this.txtMensajeChatUSU = new System.Windows.Forms.TextBox();
            this.lstChatUSU = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.UsuariosPage.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.UBloqueoDesbloqueoPage.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(957, -7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 70);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Work Sans SemiBold", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Control de Seguridad";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1406, 53);
            this.panel1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.UsuariosPage);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Work Sans Medium", 12F, System.Drawing.FontStyle.Bold);
            this.tabControl1.Location = new System.Drawing.Point(0, 54);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1058, 666);
            this.tabControl1.TabIndex = 3;
            // 
            // UsuariosPage
            // 
            this.UsuariosPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(225)))));
            this.UsuariosPage.Controls.Add(this.tabControl2);
            this.UsuariosPage.Location = new System.Drawing.Point(4, 33);
            this.UsuariosPage.Name = "UsuariosPage";
            this.UsuariosPage.Padding = new System.Windows.Forms.Padding(3);
            this.UsuariosPage.Size = new System.Drawing.Size(1050, 629);
            this.UsuariosPage.TabIndex = 0;
            this.UsuariosPage.Text = "Usuarios";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.UBloqueoDesbloqueoPage);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Font = new System.Drawing.Font("Work Sans Medium", 10F, System.Drawing.FontStyle.Bold);
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1042, 623);
            this.tabControl2.TabIndex = 0;
            // 
            // UBloqueoDesbloqueoPage
            // 
            this.UBloqueoDesbloqueoPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(225)))));
            this.UBloqueoDesbloqueoPage.Controls.Add(this.btnGuardarUBUS);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label11);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.cmbEstatusUBUS);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.txtNombreUBUS);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label10);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.txtIDUBUS);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label8);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.txtDomicilioUBUS);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label9);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label7);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.btnBuscarUsuarioUBUS);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.cmbUsuariosUSUB);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label5);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.txtNombreBusUBUS);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label4);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label3);
            this.UBloqueoDesbloqueoPage.Controls.Add(this.label2);
            this.UBloqueoDesbloqueoPage.Location = new System.Drawing.Point(4, 29);
            this.UBloqueoDesbloqueoPage.Name = "UBloqueoDesbloqueoPage";
            this.UBloqueoDesbloqueoPage.Padding = new System.Windows.Forms.Padding(3);
            this.UBloqueoDesbloqueoPage.Size = new System.Drawing.Size(1034, 590);
            this.UBloqueoDesbloqueoPage.TabIndex = 0;
            this.UBloqueoDesbloqueoPage.Text = "Bloqueo / Desbloqueo";
            // 
            // btnGuardarUBUS
            // 
            this.btnGuardarUBUS.Font = new System.Drawing.Font("Work Sans", 7.8F);
            this.btnGuardarUBUS.Location = new System.Drawing.Point(538, 466);
            this.btnGuardarUBUS.Name = "btnGuardarUBUS";
            this.btnGuardarUBUS.Size = new System.Drawing.Size(295, 27);
            this.btnGuardarUBUS.TabIndex = 18;
            this.btnGuardarUBUS.Text = "Guardar";
            this.btnGuardarUBUS.UseVisualStyleBackColor = true;
            this.btnGuardarUBUS.Click += new System.EventHandler(this.btnGuardarUBUS_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Work Sans", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(535, 322);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 16);
            this.label11.TabIndex = 17;
            this.label11.Text = "Estatus";
            // 
            // cmbEstatusUBUS
            // 
            this.cmbEstatusUBUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstatusUBUS.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEstatusUBUS.FormattingEnabled = true;
            this.cmbEstatusUBUS.Items.AddRange(new object[] {
            "Bloqueado",
            "Desbloqueado"});
            this.cmbEstatusUBUS.Location = new System.Drawing.Point(538, 341);
            this.cmbEstatusUBUS.Name = "cmbEstatusUBUS";
            this.cmbEstatusUBUS.Size = new System.Drawing.Size(292, 28);
            this.cmbEstatusUBUS.TabIndex = 16;
            // 
            // txtNombreUBUS
            // 
            this.txtNombreUBUS.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreUBUS.Location = new System.Drawing.Point(10, 402);
            this.txtNombreUBUS.Name = "txtNombreUBUS";
            this.txtNombreUBUS.ReadOnly = true;
            this.txtNombreUBUS.Size = new System.Drawing.Size(465, 27);
            this.txtNombreUBUS.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Work Sans", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 383);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 16);
            this.label10.TabIndex = 14;
            this.label10.Text = "Nombre";
            // 
            // txtIDUBUS
            // 
            this.txtIDUBUS.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDUBUS.Location = new System.Drawing.Point(10, 342);
            this.txtIDUBUS.Name = "txtIDUBUS";
            this.txtIDUBUS.ReadOnly = true;
            this.txtIDUBUS.Size = new System.Drawing.Size(465, 27);
            this.txtIDUBUS.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Work Sans", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 323);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 16);
            this.label8.TabIndex = 12;
            this.label8.Text = "ID";
            // 
            // txtDomicilioUBUS
            // 
            this.txtDomicilioUBUS.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDomicilioUBUS.Location = new System.Drawing.Point(10, 466);
            this.txtDomicilioUBUS.Name = "txtDomicilioUBUS";
            this.txtDomicilioUBUS.ReadOnly = true;
            this.txtDomicilioUBUS.Size = new System.Drawing.Size(465, 27);
            this.txtDomicilioUBUS.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Work Sans", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 447);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "Domicilio";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 292);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(294, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "Usuario a bloquear / desbloquear";
            // 
            // btnBuscarUsuarioUBUS
            // 
            this.btnBuscarUsuarioUBUS.Font = new System.Drawing.Font("Work Sans", 7.8F);
            this.btnBuscarUsuarioUBUS.Location = new System.Drawing.Point(842, 109);
            this.btnBuscarUsuarioUBUS.Name = "btnBuscarUsuarioUBUS";
            this.btnBuscarUsuarioUBUS.Size = new System.Drawing.Size(189, 27);
            this.btnBuscarUsuarioUBUS.TabIndex = 6;
            this.btnBuscarUsuarioUBUS.Text = "Buscar usuario";
            this.btnBuscarUsuarioUBUS.UseVisualStyleBackColor = true;
            this.btnBuscarUsuarioUBUS.Click += new System.EventHandler(this.btnBuscarUsuarioUBUS_Click);
            // 
            // cmbUsuariosUSUB
            // 
            this.cmbUsuariosUSUB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUsuariosUSUB.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUsuariosUSUB.FormattingEnabled = true;
            this.cmbUsuariosUSUB.Location = new System.Drawing.Point(10, 108);
            this.cmbUsuariosUSUB.Name = "cmbUsuariosUSUB";
            this.cmbUsuariosUSUB.Size = new System.Drawing.Size(349, 28);
            this.cmbUsuariosUSUB.TabIndex = 5;
            this.cmbUsuariosUSUB.SelectedIndexChanged += new System.EventHandler(this.cmbUsuariosUSUB_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Work Sans", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Usuario";
            // 
            // txtNombreBusUBUS
            // 
            this.txtNombreBusUBUS.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreBusUBUS.Location = new System.Drawing.Point(368, 108);
            this.txtNombreBusUBUS.Name = "txtNombreBusUBUS";
            this.txtNombreBusUBUS.Size = new System.Drawing.Size(465, 27);
            this.txtNombreBusUBUS.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Work Sans", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(364, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Búsqueda de usuario";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Work Sans Medium", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(6, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(385, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Bloqueo / Desbloqueo de Usuarios";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(225)))));
            this.tabPage6.Controls.Add(this.label6);
            this.tabPage6.Controls.Add(this.btnEnviarChatUSU);
            this.tabPage6.Controls.Add(this.txtMensajeChatUSU);
            this.tabPage6.Controls.Add(this.lstChatUSU);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1034, 590);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Chat";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Work Sans", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 537);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Mensaje:";
            // 
            // btnEnviarChatUSU
            // 
            this.btnEnviarChatUSU.Font = new System.Drawing.Font("Work Sans", 7.8F);
            this.btnEnviarChatUSU.Location = new System.Drawing.Point(843, 557);
            this.btnEnviarChatUSU.Name = "btnEnviarChatUSU";
            this.btnEnviarChatUSU.Size = new System.Drawing.Size(185, 27);
            this.btnEnviarChatUSU.TabIndex = 19;
            this.btnEnviarChatUSU.Text = "Enviar";
            this.btnEnviarChatUSU.UseVisualStyleBackColor = true;
            this.btnEnviarChatUSU.Click += new System.EventHandler(this.btnEnviarChatUSU_Click);
            // 
            // txtMensajeChatUSU
            // 
            this.txtMensajeChatUSU.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMensajeChatUSU.Location = new System.Drawing.Point(12, 556);
            this.txtMensajeChatUSU.Name = "txtMensajeChatUSU";
            this.txtMensajeChatUSU.Size = new System.Drawing.Size(825, 27);
            this.txtMensajeChatUSU.TabIndex = 4;
            // 
            // lstChatUSU
            // 
            this.lstChatUSU.Font = new System.Drawing.Font("Work Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstChatUSU.FormattingEnabled = true;
            this.lstChatUSU.ItemHeight = 20;
            this.lstChatUSU.Location = new System.Drawing.Point(12, 3);
            this.lstChatUSU.Name = "lstChatUSU";
            this.lstChatUSU.Size = new System.Drawing.Size(1016, 524);
            this.lstChatUSU.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(225)))));
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1050, 629);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Visitas";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(225)))));
            this.tabPage3.Location = new System.Drawing.Point(4, 33);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1050, 629);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Domicilio";
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(226)))), ((int)(((byte)(225)))));
            this.ClientSize = new System.Drawing.Size(1063, 721);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainPage";
            this.Text = "Securify";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainPage_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.UsuariosPage.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.UBloqueoDesbloqueoPage.ResumeLayout(false);
            this.UBloqueoDesbloqueoPage.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage UsuariosPage;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage UBloqueoDesbloqueoPage;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBuscarUsuarioUBUS;
        private System.Windows.Forms.ComboBox cmbUsuariosUSUB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNombreBusUBUS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGuardarUBUS;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbEstatusUBUS;
        private System.Windows.Forms.TextBox txtNombreUBUS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtIDUBUS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDomicilioUBUS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnEnviarChatUSU;
        private System.Windows.Forms.TextBox txtMensajeChatUSU;
        private System.Windows.Forms.ListBox lstChatUSU;
    }
}