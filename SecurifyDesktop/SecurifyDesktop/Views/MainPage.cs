﻿using SecurifyDesktop.Controllers;
using SecurifyDesktop.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecurifyDesktop.Views
{
    public partial class MainPage : Form
    {
        static private NetworkStream stream;
        static private StreamWriter streamw;
        static private StreamReader streamr;
        static private TcpClient client = new TcpClient();
        static private string nick = "Guardia";
        private delegate void DAddItem(String s);
        public bool firstRun = true;
        public bool cerrado = false;
        LoginPage page = new LoginPage();


        public MainPage()
        {
            InitializeComponent();
            InitializeMyComponents();
            Conectar();
        }

        //Bloquear Usuario
        private async void InitializeMyComponents()
        {
            UsuarioController usuarioController = new UsuarioController();
            List<Usuario> usuarios = new List<Usuario>();
            usuarios = await usuarioController.GetUsuarios();

            cmbUsuariosUSUB.ValueMember = "id";
            cmbUsuariosUSUB.DisplayMember = "nombre";
            cmbUsuariosUSUB.DataSource = usuarios;

        }
        private async void btnBuscarUsuarioUBUS_Click(object sender, EventArgs e)
        {
            UsuarioController usuarioController = new UsuarioController();
            List<Usuario> usuarios = new List<Usuario>();
            Usuario usuario = new Usuario();
            try
            {
                if (txtNombreBusUBUS.Text != "")
                {
                    usuario.nombre = txtNombreBusUBUS.Text;
                    usuarios = await usuarioController.GetUsuariosNombre(usuario);
                    cmbUsuariosUSUB.DataSource = usuarios;
                }
                else
                    MessageBox.Show("Inserte un valor para realizar la búsqueda");
            }
            catch (Exception ex )
            {
                
            }
        }
        private async void cmbUsuariosUSUB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (firstRun) { firstRun = false; return; }

            UsuarioController usuarioController = new UsuarioController();
            Usuario usuario = new Usuario();
            usuario.id = int.Parse(cmbUsuariosUSUB.SelectedValue.ToString());
            usuario = await usuarioController.GetUsuarioID(usuario);

            txtIDUBUS.Text = usuario.id.ToString();
            txtNombreUBUS.Text = usuario.nombre;
            if (usuario.status == 0)
                cmbEstatusUBUS.SelectedIndex = 0;
            else
                cmbEstatusUBUS.SelectedIndex = 1;

            Domicilio domicilio = new Domicilio();
            domicilio.id = usuario.idDomicilio;
            DomicilioController domicilioController = new DomicilioController();
            domicilio = await domicilioController.GetDomicilioID(domicilio);

            Calle calle = new Calle();
            calle.id = domicilio.idCalle;
            CalleController calleController = new CalleController();
            calle = await calleController.GetCalleID(calle);

            Colonia colonia = new Colonia();
            colonia.id = calle.idColonia;
            ColoniaController coloniaController = new ColoniaController();
            colonia = await coloniaController.GetColoniaID(colonia);

            txtDomicilioUBUS.Text = $"{calle.nombre} {domicilio.numero} Col. {colonia.nombre}";
        }
        private async void btnGuardarUBUS_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();
            usuario.id = int.Parse(txtIDUBUS.Text);
            if (cmbEstatusUBUS.SelectedItem.ToString() == "Bloqueado")
                usuario.status = 0;
            else
                usuario.status = 1;
            UsuarioController usuarioController = new UsuarioController();
            bool response = await usuarioController.BloquearUsuario(usuario);
            if (response == true)
                MessageBox.Show("Se ha actualizado el status del usuario");
            else
                MessageBox.Show("Ha fallado la actualización del status");
        }


        //Chat Usuario
        private void btnEnviarChatUSU_Click(object sender, EventArgs e)
        {
            streamw.WriteLine(txtMensajeChatUSU.Text);
            streamw.Flush();
            txtMensajeChatUSU.Clear();
        }
        void Listen()
        {
            while (client.Connected)
            {
                try
                {
                    this.Invoke(new DAddItem(AddItem), streamr.ReadLine());

                }
                catch
                {
                    MessageBox.Show("No se ha podido conectar al servidor de chat");
                }
            }
        }
        private void AddItem(String s)
        {
            lstChatUSU.Items.Add(s);
        }
        void Conectar()
        {
            try
            {
                client.Connect("127.0.0.1", 8000);
                if (client.Connected)
                {
                    Thread t = new Thread(Listen);

                    stream = client.GetStream();
                    streamw = new StreamWriter(stream);
                    streamr = new StreamReader(stream);

                    streamw.WriteLine(nick);
                    streamw.Flush();

                    t.Start();
                }
                else
                {
                    MessageBox.Show("Servidor no Disponible");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Servidor no Disponible");
            }
        }

        private void MainPage_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(1);
        }
    }
}
