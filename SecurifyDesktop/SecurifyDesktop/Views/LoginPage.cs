﻿using SecurifyDesktop.Controllers;
using SecurifyDesktop.Models;
using SecurifyDesktop.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecurifyDesktop
{
    public partial class LoginPage : Form
    {
        public LoginPage()
        {
            InitializeComponent();
        }
        private async void btnIniciarSesión_Click(object sender, EventArgs e)
        {
            UsuarioController usuarioController = new UsuarioController();
            Usuario usuario = new Usuario();
            try
            {
                if (txtCodigo.Text != "" && txtPassword.Text != "")
                {
                    usuario.codigo = txtCodigo.Text;
                    usuario.password = txtPassword.Text;
                    usuario = await usuarioController.LoginUsuario(usuario);
                    if (usuario.id != 0)
                    {
                        if (usuario.idPerfil == 2)
                        {
                            MainPage mainPage = new MainPage();
                            mainPage.Show();
                            txtPassword.Text = "";
                            this.Hide();
                        }
                        else
                            MessageBox.Show("Tu usuario no tiene acceso a este sistema");
                    }
                    else
                        MessageBox.Show("Código / Contraseña incorrectos");
                }
                else
                    MessageBox.Show("Faltan campos por llenar");
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
